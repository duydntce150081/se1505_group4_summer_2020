USE [master]
GO
/****** Object:  Database [MUZIC]    Script Date: 3/15/2022 8:11:55 AM ******/
CREATE DATABASE [MUZIC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MUZIC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MUZIC.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MUZIC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MUZIC_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [MUZIC] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MUZIC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MUZIC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MUZIC] SET ARITHABORT OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MUZIC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MUZIC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MUZIC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MUZIC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MUZIC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MUZIC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MUZIC] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MUZIC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MUZIC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MUZIC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MUZIC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MUZIC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MUZIC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MUZIC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MUZIC] SET RECOVERY FULL 
GO
ALTER DATABASE [MUZIC] SET  MULTI_USER 
GO
ALTER DATABASE [MUZIC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MUZIC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MUZIC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MUZIC] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MUZIC] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MUZIC] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'MUZIC', N'ON'
GO
ALTER DATABASE [MUZIC] SET QUERY_STORE = OFF
GO
USE [MUZIC]
GO
/****** Object:  UserDefinedFunction [dbo].[rmvAccent]    Script Date: 3/15/2022 8:11:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[rmvAccent] (@text nvarchar(max))
RETURNS nvarchar(max)
AS
BEGIN
	SET @text = LOWER(@text)
	DECLARE @textLen int = LEN(@text)
	IF @textLen > 0
	BEGIN
		DECLARE @index int = 1
		DECLARE @lPos int
		DECLARE @SIGN_CHARS nvarchar(100) = N'ăâđêôơưàảãạáằẳẵặắầẩẫậấèẻẽẹéềểễệếìỉĩịíòỏõọóồổỗộốờởỡợớùủũụúừửữựứỳỷỹỵýđð'
		DECLARE @UNSIGN_CHARS varchar(100) = 'aadeoouaaaaaaaaaaaaaaaeeeeeeeeeeiiiiiooooooooooooooouuuuuuuuuuyyyyydd'

		WHILE @index <= @textLen
		BEGIN
			SET @lPos = CHARINDEX(SUBSTRING(@text,@index,1),@SIGN_CHARS)
			IF @lPos > 0
			BEGIN
				SET @text = STUFF(@text,@index,1,SUBSTRING(@UNSIGN_CHARS,@lPos,1))
			END
			SET @index = @index + 1
		END
	END
	RETURN @text
END
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/15/2022 8:11:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Username] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](max) NULL,
	[IsAdmin] [bit] NULL,
	[Date] [datetime] NULL,
	[Status] [int] NULL,
	[Avatar] [nvarchar](max) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 3/15/2022 8:11:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Detail] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[Priority] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[WebTracking](
	[Username] [nvarchar](100) NULL,
	[ContextURL] [nvarchar](100) NULL,
	[Date] [datetime] DEFAULT GETDATE()
)


insert into WebTracking (Date) values('2022-03-19 12:05:30')
insert into WebTracking (Date) values('2022-03-19 14:05:30')
insert into WebTracking (Date) values('2022-03-19 15:05:30')
insert into WebTracking (Date) values('2022-03-19 16:05:30')
insert into WebTracking (Date) values('2022-03-19 13:05:30')
insert into WebTracking (Date) values('2022-03-19 11:05:30')
insert into WebTracking (Date) values('2022-03-19 14:05:30')
insert into WebTracking (Date) values('2022-03-19 12:05:30')
insert into WebTracking (Date) values('2022-03-19 13:05:30')
insert into WebTracking (Date) values('2022-03-19 09:05:30')
insert into WebTracking (Date) values('2022-03-19 14:05:30')
insert into WebTracking (Date) values('2022-03-19 15:05:30')
insert into WebTracking (Date) values('2022-03-19 19:05:30')
insert into WebTracking (Date) values('2022-03-19 18:05:30')
insert into WebTracking (Date) values('2022-03-19 17:05:30')
insert into WebTracking (Date) values('2022-03-19 12:05:30')
insert into WebTracking (Date) values('2022-03-19 11:05:30')

insert into WebTracking (Date) values('2022-03-20 09:05:30')
insert into WebTracking (Date) values('2022-03-19 08:05:30')
insert into WebTracking (Date) values('2022-03-19 12:05:30')
insert into WebTracking (Date) values('2022-03-19 13:05:30')
insert into WebTracking (Date) values('2022-03-19 14:05:30')
insert into WebTracking (Date) values('2022-03-19 12:05:30')
insert into WebTracking (Date) values('2022-03-19 14:05:30')
insert into WebTracking (Date) values('2022-03-19 12:05:30')
insert into WebTracking (Date) values('2022-03-19 15:05:30')
insert into WebTracking (Date) values('2022-03-19 15:05:30')
insert into WebTracking (Date) values('2022-03-19 08:05:30')
insert into WebTracking (Date) values('2022-03-19 19:05:30')
insert into WebTracking (Date) values('2022-03-19 09:05:30')
insert into WebTracking (Date) values('2022-03-19 10:05:30')
insert into WebTracking (Date) values('2022-03-19 10:05:30')
insert into WebTracking (Date) values('2022-03-19 12:05:30')
insert into WebTracking (Date) values('2022-03-19 11:05:30')

insert into WebTracking (Date) values('2022-03-20 09:05:30')
insert into WebTracking (Date) values('2022-03-20 08:05:30')
insert into WebTracking (Date) values('2022-03-20 12:05:30')
insert into WebTracking (Date) values('2022-03-20 13:05:30')
insert into WebTracking (Date) values('2022-03-20 14:05:30')
insert into WebTracking (Date) values('2022-03-20 12:05:30')
insert into WebTracking (Date) values('2022-03-20 14:05:30')
insert into WebTracking (Date) values('2022-03-20 12:05:30')
insert into WebTracking (Date) values('2022-03-20 15:05:30')
insert into WebTracking (Date) values('2022-03-20 15:05:30')
insert into WebTracking (Date) values('2022-03-20 08:05:30')
insert into WebTracking (Date) values('2022-03-20 19:05:30')
insert into WebTracking (Date) values('2022-03-20 09:05:30')
insert into WebTracking (Date) values('2022-03-20 10:05:30')
insert into WebTracking (Date) values('2022-03-20 10:05:30')
insert into WebTracking (Date) values('2022-03-20 12:05:30')
insert into WebTracking (Date) values('2022-03-20 11:05:30')

GO
/****** Object:  Table [dbo].[Playlist]    Script Date: 3/15/2022 8:11:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Playlist](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Thumbnail] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[IsPublic] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaylistSong]    Script Date: 3/15/2022 8:11:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistSong](
	[PlaylistId] [int] NOT NULL,
	[SongId] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PlaylistId] ASC,
	[SongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaylistTracking]    Script Date: 3/15/2022 8:11:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistTracking](
	[Username] [nvarchar](100) NOT NULL,
	[PlaylistId] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Song]    Script Date: 3/15/2022 8:11:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Song](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Src] [nvarchar](max) NULL,
	[Thumbnail] [nvarchar](max) NULL,
	[Cover] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Artist] [nvarchar](max) NULL,
	[Genre] [nvarchar](max) NULL,
	[Lyrics] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[Duration] [int] NOT NULL,
	[IsPublic] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SongTracking]    Script Date: 3/15/2022 8:11:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SongTracking](
	[Username] [nvarchar](100) NOT NULL,
	[SongId] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'admin', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 1, CAST(N'2022-03-04T23:14:25.150' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Admin Nè', N'admin@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'canhdm', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.197' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Poi Minh Cảnh', N'canhdm@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duydn', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-02-26T15:50:59.090' AS DateTime), 0, N'/img/defaultAvatar.jpg', NULL, NULL)
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duydnt', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.660' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Đặng Ngọc Tùng Duy', N'duydnt@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duynk', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.697' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Nguyễn Khánh Duy', N'duynk@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'nganhvu', N'z2eNw6U5X-zpCteH4rnzh4e74tgu6Q3VDeejop3kaHE', 0, CAST(N'2022-03-10T17:33:09.097' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'nganhvu', N'anhvu2001ct@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'quynhln', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.337' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Lê Nhật Quỳnh', N'quynhln@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'vuna', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.497' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Nguyễn Anh Vũ', N'vuna@gmail.com')
GO
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (1, N'canhdm', N'Nghe Nhạc Bị Lỗi', N'Không nghe được nhạc', CAST(N'2022-03-04T23:15:21.713' AS DateTime), 0)
INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (2, N'quynhln', N'Đổi Mật Khẩu', N'Không đổi mật khẩu được', CAST(N'2022-03-04T23:15:21.757' AS DateTime), 1)
INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (3, N'vuna', N'Upload Nhạc', N'Không upload nhạc được', CAST(N'2022-03-04T23:15:21.787' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[Feedback] OFF
GO
SET IDENTITY_INSERT [dbo].[Playlist] ON 

INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (1, N'canhdm', N'https://yt3.ggpht.com/i2BBir9626AxLzMHuyF1sx4WiHV-CqipS0EC9c2UklgT6bC0inZ-XvKVHgsjckxKl2_XjV_uyDk=s900-c-k-c0x00ffffff-no-rj', N'Sơn Tùng M-TP Collection', N'Playlist được ra mắt vào những năm thập niên cuối thế kỉ 21...', CAST(N'2022-03-04T15:30:00.000' AS DateTime), 1)
INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (2, N'nhatquynh', N'https://tophinhanhdep.com/wp-content/uploads/2021/10/Thumbnail-Wallpapers.jpg', N'Jack Collection', N'Bộ sưu tập 51 hình nền và hình nền Hình thu nhỏ hàng đầu có sẵn để tải xuống miễn phí. Chúng tôi hy vọng bạn thích bộ sưu tập hình ảnh HD ngày càng tăng của chúng tôi để sử dụng làm hình nền hoặc màn hình chính cho điện thoại thông minh hoặc máy tính của bạn. Vui lòng liên hệ chúng tôi nếu bạn muốn xuất bản một Hình nhỏ hình nền trên trang web của chúng tôi.', CAST(N'2022-03-04T15:30:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Playlist] OFF
GO
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 1, CAST(N'2022-03-10T11:56:57.070' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 2, CAST(N'2022-03-10T11:57:01.660' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 3, CAST(N'2022-03-10T11:57:05.050' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 5, CAST(N'2022-03-10T11:57:10.063' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 6, CAST(N'2022-03-10T11:57:11.350' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 16, CAST(N'2022-03-15T08:10:17.773' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 17, CAST(N'2022-03-15T08:10:19.720' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 18, CAST(N'2022-03-15T08:10:21.527' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 19, CAST(N'2022-03-15T08:10:23.660' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 20, CAST(N'2022-03-15T08:10:29.807' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 1, CAST(N'2022-03-10T11:57:14.207' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 2, CAST(N'2022-03-10T11:57:16.343' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 3, CAST(N'2022-03-10T11:57:18.913' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 4, CAST(N'2022-03-10T11:57:20.963' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 5, CAST(N'2022-03-10T11:57:24.350' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 6, CAST(N'2022-03-10T11:57:25.693' AS DateTime))
GO
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-10T12:01:54.740' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 2, CAST(N'2022-03-10T12:02:05.983' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'vuna', 1, 3, CAST(N'2022-03-10T12:02:11.547' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-10T12:02:17.583' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 2, CAST(N'2022-03-10T12:02:25.263' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Song] ON 

INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (1, N'minhcanh', N'https://ik.imagekit.io/nav26/MUZIC/song/ENGLISH_I_Wanna__Go_Home_KonoSuba__Akane_Sasu_Sora__3khnVgHth5R1.mp3?ik-sdk-version=javascript-1.4.3&updatedAt=1646412304358', N'https://yt3.ggpht.com/ytc/AKedOLRkY5n3Hd-EXXEpeUPp4INtDJTT_awisaAOhndN1g=s900-c-k-c0x00ffffff-no-rj', N'https://www.google.com/url?sa=i&url=https%3A%2F%2Flaodong.vn%2Fgiai-tri%2Fson-tung-m-tp-dung-vi-tri-bao-nhieu-trong-top-nam-than-dep-nhat-chau-a-765130.ldo&psig=AOvVaw31EZJba7Iw9zueN25H6ZQU&ust=1646225227850000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCICbhYL5pPYCFQAAAAAdAAAAABAI', N'Muộn rồi mà sao còn', N'Son Tung MTP', N'VPop', N'"Muộn rồi mà sao còn
Nhìn lên trần nhà rồi quay ra, lại quay vào
Nằm trằn trọc vậy đến sáng mai
Ôm tương tư nụ cười của ai đó
Làm con tim ngô nghê như muốn khóc oà
Vắt tay lên trên trán mơ mộng
Được đứng bên em trong nắng xuân hồng
Một giờ sáng
Trôi qua trôi nhanh kéo theo ưu phiền miên man
Âm thầm gieo tên em vẽ lên hi vọng
Đúng là yêu thật rồi
Còn không thì hơi phí này
Cứ thế loanh quanh, loanh quanh, loanh quanh
Lật qua lật lại hai giờ"', N'Đây là bài hát của Sơn Tùng M-TP', CAST(N'2022-01-03T00:00:00.000' AS DateTime), 180, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (2, N'nhatquynh', N'https://ik.imagekit.io/nav26/MUZIC/song/CHiCO_with_HoneyWorks%E5%B9%B8%E3%81%9B_WksW1gKxjG.mp3?ik-sdk-version=javascript-1.4.3&updatedAt=1646405593299', N'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.youtube.com%2Fchannel%2FUClyA28-01x4z60eWQ2kiNbA&psig=AOvVaw31EZJba7Iw9zueN25H6ZQU&ust=1646225227850000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCICbhYL5pPYCFQAAAAAdAAAAABAD', N'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.youtube.com%2Fchannel%2FUClyA28-01x4z60eWQ2kiNbA&psig=AOvVaw31EZJba7Iw9zueN25H6ZQU&ust=1646225227850000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCICbhYL5pPYCFQAAAAAdAAAAABAD', N'Âm thầm bên em', N'Son Tung MTP', N'Vpop', NULL, N'"Khi bên anh em thấy điều chi?
Khi bên anh em thấy điều gì?
Nước mắt rơi gần kề làn mi
Chẳng còn những giây phút
Chẳng còn những ân tình
Gió mang em rời xa nơi đây
Khi xa anh em nhớ về ai?
Khi xa anh em nhớ một người
Chắc không phải một người như anh
Người từng làm em khóc
Người từng khiến em buồn
Buông bàn tay, rời xa lặng thinh bước đi
Hạt mưa rơi bủa vây trái tim hiu quạnh
Ngàn yêu thương vụt tan bỗng xa
Người từng nói ở bên cạnh anh mỗi khi anh buồn
Cớ sao giờ lời nói kia như gió bay
Đừng bỏ rơi bàn tay ấy bơ vơ mà
Một mình anh lặng im chốn đây
Yêu em âm thầm bên em
Yêu thương không còn nơi đây
Anh mang tình buồn theo mây
Cơn mơ về mong manh câu thề
Tan trôi qua mau quên đi phút giây
Mưa rơi trên đôi mi qua lối vắng
Ánh sáng mờ buông lơi làn khói trắng
Bóng dáng em, nụ cười ngày hôm qua, kí ức có ngủ quên chìm trong màn sương đắng? (Anh làm em khóc)
Anh nhớ giọt nước mắt sâu lắng (anh khiến em buồn)
Anh nhớ nỗi buồn của em ngày không nắng
Buông bàn tay, rời xa lặng thinh bước đi
Hạt mưa rơi bủa vây trái tim hiu quạnh
Ngàn yêu thương vụt tan bỗng xa
Người từng nói ở bên cạnh anh mỗi khi anh buồn
Cớ sao giờ lời nói kia như gió bay?
Bàn tay bơ vơ mà
Cầm bông hoa chờ mong nhớ thương
Làm sao quên người ơi, tình anh mãi như hôm nào
Vẫn yêu người và vẫn mong em về đây
Giọt nước mắt tại sao cứ lăn rơi hoài?
Ở bên anh chỉ có đớn đau
Thì anh xin nhận hết ngàn đau đớn để thấy em cười
Dẫu biết giờ người đến không như giấc mơ
Yêu em âm thầm bên em
Yêu em âm thầm bên em
Thì anh xin nhận hết ngàn đau đớn để thấy em cười
Dẫu biết giờ người đến không như giấc mơ
Yêu em âm thầm bên em"', CAST(N'2021-03-02T00:00:00.000' AS DateTime), 150, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (4, N'nganhvu', N'http', N'/img/defaultSongThumbnail.jpg', N'/img/defaultSongCover.jpg', N'title', N'artist', NULL, NULL, NULL, CAST(N'2022-03-07T10:51:36.763' AS DateTime), -1, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (5, N'nganhvu', N'http', N'/img/defaultSongThumbnail.jpg', N'/img/defaultSongCover.jpg', N'title', NULL, NULL, NULL, NULL, CAST(N'2022-03-07T10:52:00.090' AS DateTime), 0, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (6, N'nganhvu', N'http', N'/img/defaultSongThumbnail.jpg', N'/img/defaultSongCover.jpg', N'title', NULL, NULL, NULL, NULL, CAST(N'2022-03-07T10:56:09.583' AS DateTime), 0, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (13, N'admin', N'https://ik.imagekit.io/nav26/MUZIC/song/ChiecKhanGioAmPianoVersion-AnCoong-2890378_Ge4ktHoNYR3.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Chiếc Khăn Gió Ấm (Piano Version)', N'An Coong ', N'Không Lời', NULL, NULL, CAST(N'2022-03-10T17:06:36.823' AS DateTime), 297, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (15, N'admin', N'https://ik.imagekit.io/nav26/MUZIC/song/ChiecKhanGioAmPianoVersion-AnCoong-2890378_8m1c4Zjbo.mp3', N'/img/upload-thumbnail.jpg', N'https://ik.imagekit.io/nav26/MUZIC/img/user_function_6fLagtUB7.png', N'Chiếc Khăn Gió Ấm (Piano Version)', N'An Coong ', N'Không Lời', N'Chiếc khăn gió ấm', NULL, CAST(N'2022-03-10T22:29:14.147' AS DateTime), 297, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (16, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/YourSmile-Crush_SwlGyeTHa.mp4', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Your Smile', N'Crush', N'Vpop', N'Từng ngày từng tháng trôi qua thành năm
Em vẫn mong chờ tình yêu
Bước qua bao cảm xúc, ôi nỗi đau em mang thật nhiều
Giọt lệ buồn trên đôi mắt hiền
,cõi lòng như cây xơ xác vậy
Thấy em buồn rầu nhưng chẳng mấy ai thương em

Và là lá la là la, em vẫn hát thật nhiều bài ca
Đôi chân bé xinh trẻ con, đi khắp nơi em không về nhà
Vẫn thấy em xinh thường mà
Dặn lòng mong người đừng yêu ai
Chỉ muốn nắm tay em thôi
Khóa đôi môi không cho một ai

Dắt tay em đi qua rừng người ở trên phố đông
Chìm đắm trong ánh mắt em
Một nụ cười làm anh ngất ngây
Ôi sao mà khó thế
Tim này mày đập nhanh quá
Hay là tại mình thôi thúc?
Ôi hóa ra yêu rồi này

Để anh lau nước mắt
Chẳng để giọt buồn trên khóe mi
Đuổi hết những cón đau nhói của từng người làm ray rứt em
Rồi thoáng qua những con phố
Mình cùng chuyện trò bên ánh trăng
Mình cứ yêu đi để thấy một ngày đẹp trời sẽ ghé thăm
Girl I want to see your smile Yeah yeah yeah yeah
Sẽ không có những đau buốt No no no no
I think Im fall in love with you
I wanna see your smile girl, see your smile yeah yeah

[Rap]Em là cô gái của bao vụn vỡ
Là ánh sao buồn của bầu trời khuya
Là áng mây đen những ngày chuyển trời
Vương miếng miếng đời
, tình 10 lời chia
Đời vốn không yêu tình cảm chân thành
Em vẫn an lành nở nụ cười duyên
Họ chỉ yêu em bằng tâm kẻ ác
Chứ quan tâm gì linh hồn thần tiên

Nhưng em vẫn cười, miệng vu vơ hát
Vẫn ngồi trông đợi về một ngày mai
Em ước mình biến thành lọ chai nhỏ
Lênh đênh trên biển tình chất đầy vai
Em vẫn muốn yêu
Nhưng không muốn nhiều
Cũng chưa từng trách người nào sai
Anh cũng ngây ngô vì nụ cười đó
Nhưng không yêu kiểu rót mật vào tai

Và nói em ơi sao mà em lại yêu đậm sâu thế
Em nói anh nghe: "Em thật tình đâu có muốn thế"
Mình vốn sinh ra trên cuộc đời để tìm tình yêu mà
Vậy thế cho anh xin 1 lần được yêu bởi em nha

Bật người tròn mắt
Bất ngờ em nhìn anh
2 má em ửng đỏ
Tim đã bắt đầu báo động
Chắc do là gấp rút
Anh lại sợ em đi mất
Cũng đâu hề sửa soạn trước
Chỉ quần tây và áo phông
Có thể mình khiêu vũ ngay tại dưới ánh đèn đường
Dù thành phố này đông người em cũng không nên lo đâu
Vì đây là khoảnh khắc đẹp nhất của một đời người
Khi biết mình đã thật sự có được 1 tình yêu đầu', N'Qua hay va day tinh cam ~', CAST(N'2022-03-13T21:20:37.453' AS DateTime), 147, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (17, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/MuonRoiMaSaoCon-SonTungMTP-7011803_ILbbQypwp.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Muộn rồi mà sao còn', N'Sơn Tùng MTP', N'Vpop', N'Muộn rồi mà sao còn nhìn lên trần nhà rồi quay ra lại quay vào
Nằm trằn trọc vậy đến sáng mai
Ôm tương tư, nụ cười của ai đó
Làm con tim ngô nghê như muốn khóc òa
Vắt tay lên trên trán mơ mộng
Được đứng bên em trong nắng xuân hồng
1 giờ sáng trôi qua trôi nhanh kéo theo ưu phiền miên man
Âm thầm gieo tên em vẽ lên hi vọng
Đúng là yêu thật rồi còn không thì hơi phí này cứ thế loanh quanh loanh
Quanh loanh quanh lật qua lật lại (2 giờ)
Những ngôi sao trên cao là người bạn tâm giao
Lắng nghe anh luyên thuyên về một tình đầu đẹp tựa chiêm bao
Có nghe thôi đã thấy ngọt ngào
Đủ biết anh si mê em nhường nào
Ít khi văn thơ anh dạt dào bụng đói nhưng vui quên luôn cồn cào
Nắm đôi tay kiêu sa được một lần không ta
Nghĩ qua thôi con tim trong anh đập tung lên rung nóc rung nhà
Hóa ra yêu đơn phương một người
Hóa ra khi tơ vương một người 3 giờ đêm vẫn ngồi cười

Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong cơn mơ trong cơn mơ trong cơn mơ trong cơn mơ
Có thế cũng khiến anh vui điên lên ngỡ như em đang bên
Chắp bút đôi ba câu thơ ngọt ngào muốn em đặt tên
Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong giấc mơ trong cơn mơ trong cơn mơ trong cơn mơ
Yêu đến vậy thôi phát điên rồi làm sao giờ

[Rap:]
Chịu!
Đêm nay không ngủ
Tay kê lên tủ
Miên man anh tranh thủ chơi với suy tư bao nhiêu cho đủ
Yêu em ngu ngơ mình tôi
Yêu không quan tâm ngày trôi
Yêu ánh mắt bờ môi
Yêu đơn phương vậy thôi
Lại còn chối
Con tim thẹn thùng đập lạc lối liên hồi
Đừng chày cối
Miệng cười cả ngày vậy là chết toi rồi
Ngày càng nhiều thêm
Tình yêu cho em ngày càng nhiều thêm
Muốn nắm đôi bàn tay đó một lần
Du dương chìm sâu trong từng câu ca dịu êm

[Ver 2:]
Những ngôi sao trên cao
Là người bạn tâm giao
Lắng nghe anh luyên thuyên về một tình đầu đẹp tựa chiêm bao
Có nghe thôi đã thấy ngọt ngào
Đủ biết anh si mê em nhường nào
Ít khi văn thơ anh dạt dào bụng đói nhưng vui quên luôn cồn cào
Nắm đôi tay kiêu sa được một lần không ta
Nghĩ qua thôi con tim trong anh đập tung lên rung nóc rung nhà
Hóa ra yêu đơn phương một người
Hóa ra khi tơ vương một người 3 giờ đêm vẫn ngồi cười

Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong cơn mơ trong cơn mơ trong cơn mơ trong cơn mơ
Có thế cũng khiến anh vui điên lên ngỡ như em đang bên
Chắp bút đôi ba câu thơ ngọt ngào muốn em đặt tên
Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong giấc mơ trong cơn mơ trong cơn mơ trong cơn mơ
Yêu đến vậy thôi phát điên rồi làm sao giờ

Em xinh như một thiên thần
Như một thiên thần
Như một thiên thần
Ngỡ như em là thiên thần
Em xinh như một thiên thần
Như một thiên thần

Em xinh như một thiên thần
Như một thiên thần
Như một thiên thần
Ngỡ như em là thiên thần
Ngỡ như ngỡ như ngỡ như ngỡ như ngỡ như

Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong cơn mơ trong cơn mơ trong cơn mơ trong cơn mơ
Có thế cũng khiến anh vui điên lên ngỡ như em đang bên
Chắp bút đôi ba câu thơ ngọt ngào muốn em đặt tên
Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong giấc mơ trong cơn mơ trong cơn mơ trong cơn mơ
Yêu đến vậy thôi phát điên rồi làm sao giờ', N'Qua hay va day tinh cam ~', CAST(N'2022-03-13T21:20:37.453' AS DateTime), 276, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (18, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/SaiGonDauLongQua-HuaKimTuyenHoangDuyen-6992977_7rZM2jjMr.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Sài Gòn Đau Lòng Quá', N'Hoàng Duyên', N'Vpop', N'Cầm tấm vé trên tay
Em bay đến nơi xa
Sài Gòn đau lòng quá
Toàn kỷ niệm chúng ta.
Phải đi xa đến đâu?
Thời gian quên mất bao lâu?
Để trái tim em bình yên như ngày đầu tiên...

Mình đã từng hứa
Bên nhau hết tháng năm dài
Yêu đến khi ngừng hơi thở
Đến khi ngừng mơ...
Nắm chặt tay đi hết nhân thế này
Chân trời hằn chân ta
Vô tận là chúng ta...

Mình đã từng hứa
Đi qua hết bao thăng trầm
Cho dẫu mai này xa rời
Vẫn không hề đổi dời...
Có ngờ đâu, đã sớm vỡ tan tành
Nhặt từng mảnh vỡ xếp vào vali...

Cứ càng yêu, cứ càng đau
Cứ càng quên rồi lại muốn đi thật nhiều
Tokyo hay Seoul
Paris hay New York
Đi càng xa, càng không thể quên...

Cầm tấm vé trên tay
Em bay đến nơi xa
Sài Gòn đau lòng quá
Toàn kỷ niệm chúng ta.
Phải đi xa đến đâu?
Thời gian quên mất bao lâu?
Để trái tim em bình yên như ngày đầu tiên...

Ngày tôi chưa từng biết tôi sẽ yêu em nhiều như thế này
Để rồi khi ta cách xa tim này nát ra
Ngày người chưa đến mang theo giấc mơ, rồi lại bỏ rơi lúc tôi đang chờ...
Chờ người đến dịu xoa tổn thương tôi đã từng...', N'Qua hay va day tinh cam ~', CAST(N'2022-03-13T21:20:37.453' AS DateTime), 309, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (19, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/Me-HoangDuyen-7129356__0Ya2tK8w37.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Mê', N'Hoàng Duyên', N'Vpop', N'Verse 1
Có thể là ta có duyên gặp nhau
Chắc nên là tôi mới phải lòng anh
Người con trai có
Nụ cười duyên đó
Và đôi mắt cười là anh

Chẳng thể nào quên những khi gần anh
Có lẽ nào anh đã mê hoặc tôi
Chẳng thể từ chối
Chẳng thể nói dối
Rằng tôi thích anh rồiiii

Hù hú hu ôi chàng đẹp nhất thế gian
Người có phải thiên thần đến thế gian
Hù hú hu ôi chàng đẹp nhất thế gian
Chắc là, chắc là, chắc là thiên thần

Hù hú hu ôi chàng đẹp nhất thế gian
Người đến đây mê hoặc khắp thế gian
Hù hú hu ôi chàng đẹp nhất thế gian
Thích người bởi vì, bởi vì

Chorus
Bởi vì tôi đã mê anh rồi
Vẻ đẹp kia làm tôi u mê rồi
Thề rằng đó giờ chưa từng mê ai
Như anh

Ngoài kia có hàng trăm lối về
Mà nhà tôi chỉ một lối để về
Là lối vào tim của anh
Knock knock... mở cửa em vào

Verse 2
Anh gì ơi
Sao cứ lang thang, lang thang, lang thang
Lạc vào trái tim tôi hoài
Lại quên lối về
Người gì đâu sao mà kì cục quá
Đến nhà người ta mà chẳng chịu về

Anh gì ơi
Tôi lỡ thương anh, thương anh, thương anh,
Từng ngày đến cả ngây người
Mà chẳng hay gì
Lạy trời cao xin người tặng cho tôi
Người tôi hằng ao ước', N'Qua hay va day tinh cam ~', CAST(N'2022-03-13T21:20:37.453' AS DateTime), 219, 1)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (20, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/MotCuLua-BichPhuong-6288019_I35ok6hUC.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Một Cú Lừa', N'Bích Phương', N'Vpop', N'Vậy là tròn một năm bên nhau
Ngọt ngào rực rỡ như đang được sống lại tình đầu
Người ta cứ khen anh đa tình đến thế
Mà sao lúc yêu em si tình đến thế

Bọn mình thật hợp mà phải không anh ơi
Một người ưa nói như em lại thích người kiệm lời
Người mà lúc bên em hay ở bên ai thì cũng tắt chuông điện thoại

Lúc anh chạm môi cô ta anh có ngần ngại
Có ngần ngại không hay miên man nhớ mãi
Trả lời đi hương nước hoa thơm mùi gì len giữa chúng ta
Em trao anh con tim sao anh trao cho em một cú lừa

La la la la là lừa
La la la la là lừa
La la la la là lừa

Nào là mình đừng buông tay ra
Nào là anh ước hai ta rồi sẽ về một nhà
Người ta cứ khen anh đa tình đến thế
Mà sao lúc yêu em si tình...

Một người phải vờ như ngây thơ
Một người nói dối quá tồi
Tình yêu với em như ngôi đền thiêng liêng
Giờ em chẳng muốn cầu nguyện

Biết đâu được hai ta sớm có kết cục buồn
Nỗi đau này em đưa cho cơn gió cuốn
Điều gì xảy ra với em mong rằng đừng xảy ra với anh
Thôi ta chia tay đi nhưng riêng em sẽ không phải tiếc gì

La la la la là lừa
La la la la là lừa
La la la la là lừa

Lúc anh chạm môi cô ta anh có ngần ngại
Có ngần ngại không hay miên man nhớ mãi
Điều gì đã quyến rũ anh, ôi điều gì đã đánh cắp anh
Em trao anh con tim sao anh trao cho em...

Biết đâu được hai ta sớm có kết cục buồn
Nỗi đau này em đưa cho cơn gió cuốn
Điều gì xảy ra với em mong rằng đừng xảy ra với anh
Em trao anh con tim sao anh trao cho em một cú lừa', N'Qua hay va day tinh cam ~', CAST(N'2022-03-13T21:20:37.453' AS DateTime), 209, 1)
SET IDENTITY_INSERT [dbo].[Song] OFF
GO
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-02T00:00:00.000' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 2, CAST(N'2022-11-02T00:00:00.000' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 3, CAST(N'2022-01-02T00:00:00.000' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-05-01T00:00:00.000' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 2, CAST(N'2022-03-05T00:03:06.773' AS DateTime))
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ('/img/defaultAvatar.jpg') FOR [Avatar]
GO
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT ((0)) FOR [Priority]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT ('/img/defaultPlaylistThumbnail.jpg') FOR [Thumbnail]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT ((1)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[PlaylistSong] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[PlaylistTracking] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Thumbnail]  DEFAULT ('/img/defaultSongThumbnail.jpg') FOR [Thumbnail]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Cover]  DEFAULT ('/img/defaultSongCover.jpg') FOR [Cover]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_IsPublic]  DEFAULT ((1)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[SongTracking] ADD  CONSTRAINT [DF_SongTracking1_Date]  DEFAULT (getdate()) FOR [Date]
GO
USE [master]
GO
ALTER DATABASE [MUZIC] SET  READ_WRITE 
GO
