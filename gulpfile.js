const gulp = require("gulp");
const rename = require("gulp-rename");
const sass = require("gulp-sass")(require("sass"));
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const fs = require("fs/promises");
const pathz = require("path");

const stylePath = {
    src: "styles/**/*.scss",
    dest: "./"
};

function pathNoParent(path) {
    return pathz.join(...path.split("\\").slice(1));
}

async function remove(filePath) {
    let dirName = pathNoParent(pathz.dirname(filePath));
    let baseName = pathz.basename(filePath, ".scss");
    let extName = ".css";
    let path = `${dirName}\\${baseName}${extName}`;
    await fs.rm(path, {force: true});
    console.log(`remove >> ${path}`);
}

function style(filePath) {
    return gulp.src(filePath)
        .pipe(sass.sync().on("error", sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(rename(path => {
            path.dirname = pathNoParent(path.dirname);
            console.log(`change >> ${path.dirname}\\${path.basename}${path.extname}`);
        }))
        .pipe(gulp.dest(stylePath.dest));
}

function styleAll() {
    return gulp.src(stylePath.src)
        .pipe(sass.sync().on("error", sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(gulp.dest(stylePath.dest));
}

function watch() {
    gulp.watch(stylePath.src).on("change", style);
    gulp.watch(stylePath.src).on("unlink", remove);
}

module.exports = { styleAll, watch }