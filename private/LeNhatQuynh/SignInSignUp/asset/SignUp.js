var eyeChange1 = document.querySelector("#eye-change1");
var pass1 = document.querySelector("#password1");
var eyeChange2 = document.querySelector("#eye-change2");
var pass2 = document.querySelector("#password2");
var inputData = document.querySelectorAll(".input-data");
var checkLabel = document.querySelector(".remember-label");
var checkBox = document.querySelector(".check");

checkLabel.addEventListener("click", function () {
  (checkBox.checked == true)?checkBox.checked = false:checkBox.checked = true;
})


eyeChange1.addEventListener("click", function () {
  if (eyeChange1.classList.contains("fa-eye-slash")) {
    eyeChange1.classList.remove("fa-eye-slash");
    eyeChange1.classList.add("fa-eye");
    pass1.type = "text";
  } else if (eyeChange1.classList.contains("fa-eye")) {
    eyeChange1.classList.remove("fa-eye");
    eyeChange1.classList.add("fa-eye-slash");
    pass1.type = "password";
  }
});
eyeChange2.addEventListener("click", function () {
  if (eyeChange2.classList.contains("fa-eye-slash")) {
    eyeChange2.classList.remove("fa-eye-slash");
    eyeChange2.classList.add("fa-eye");
    pass2.type = "text";
  } else if (eyeChange2.classList.contains("fa-eye")) {
    eyeChange2.classList.remove("fa-eye");
    eyeChange2.classList.add("fa-eye-slash");
    pass2.type = "password";
  }
});

inputData.forEach((e)=>{
e.addEventListener("focus",function(){
    e.parentNode.style.borderColor = '#f46530';
})
e.addEventListener("blur",function(){
    e.parentNode.style.borderColor = '#c0c0c0';
})
});
