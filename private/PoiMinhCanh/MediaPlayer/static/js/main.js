var queue = document.querySelector(".media-queue");
var queueCloseBtn = document.querySelector(".queue-close-btn");
var queueToggleBtn = document.querySelector(".media");

queueCloseBtn.addEventListener("click", function (e) {
    queue.style.transform = "translateX(100%)";
});

queueToggleBtn.addEventListener("click", function (e) {
    if (queue.style.transform === "translateX(100%)") {
        queue.style.transform = "translateX(0)";
    } else queue.style.transform = "translateX(100%)";
});

var mediaPlayer = document.querySelector(".media-player");
var toggleMediaPlayerBtn = document.querySelector(".toogle-media-player");

toggleMediaPlayerBtn.addEventListener("click", function (e) {
    mediaPlayer.classList.toggle("media-player-narrow");

    let icon = toggleMediaPlayerBtn.querySelector("i");
    icon.classList.toggle("fa-arrow-circle-right");
    icon.classList.toggle("fa-arrow-circle-left");
});
