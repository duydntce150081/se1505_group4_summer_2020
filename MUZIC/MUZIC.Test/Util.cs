﻿global using System;
global using MUZIC.Model;
global using MUZIC.App.Service;
global using NUnit.Framework;
global using System.Text.Json;
using System.Collections.Generic;
using System.IO;

namespace MUZIC.Test
{
    public class Util
    {
        public static List<JsonElement[]> GetData(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory());
            dir = dir.Parent!.Parent!.Parent!;
            path = $@"{dir}\testcase\{path}";

            var json = JsonDocument.Parse(File.ReadAllText(path));
            return json.Deserialize<List<JsonElement[]>>()!;
        }
    }
}
