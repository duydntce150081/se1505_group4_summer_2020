using System.Threading.Tasks;

namespace MUZIC.Test;

[TestFixture]
public class SettingTest
{
    [SetUp]
    public void Setup()
    {
        MUZIC.App.TestHelper.Enable();
    }
   
    [TestCaseSource(typeof(Util), "GetData", new object[] { "setting/updatePassword.json" })]
    public void UpdatePassword(JsonElement[] p)
    {
        Assert.Ignore();

        string username = p[0].GetString()!;
        string oldpass = p[1].GetString()!;
        string newpass = p[2].GetString()!;
        Assert.DoesNotThrowAsync(async () =>
        {
            await SettingService.UpdatePassword(username, oldpass, newpass);
        });
    }
}