﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MUZIC.Model
{
    public class SongChoosen
    {
        public Song Song { get; set; } = new Song();
        public bool Choosen { get; set; } = false;
    }
}
