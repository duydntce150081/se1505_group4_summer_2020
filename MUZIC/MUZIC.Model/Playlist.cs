﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MUZIC.Model
{
    public class Playlist
    {
        public int Id { get; set; }
        public string Username { get; set; } = null!;
        public string Thumbnail { get; set; } = null!;
        public string Title { get; set; } = null!;
        public string Description { get; set; } = null!;
        public DateTime Date { get; set; }
        public bool IsPublic { get; set; } = true;

    }
}
