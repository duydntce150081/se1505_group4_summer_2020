﻿namespace MUZIC.Model
{
    public class TopSong
    {
        public int Id { get; set; }
        public string? Username { get; set; }
        public string? Src { get; set; }
        public string? Thumbnail { get; set; }
        public string? Cover { get; set; }
        public string? Title { get; set; }
        public string? Artist { get; set; }
        public string? Genre { get; set; }
        public string? Lyrics { get; set; }
        public string? Description { get; set; }
        public DateTime Date { get; set; }
        public int Duration { get; set; }
        public bool IsPublic { get; set; } = true;
        public int Status { get; set; }
        public int cnt { get; set; }
    }
}
