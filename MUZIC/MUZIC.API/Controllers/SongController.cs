﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using MUZIC.Model;
using System.Text.Json;

namespace MUZIC.API.Controllers
{
    public class SongController : ConBase
    {

        [HttpGet("get/{songId:int}")]
        public async Task<ActionResult<object>> get(int songId)
        {
            var li = (await Program.Sql.QueryAsync<Song>("select * from Song where Id=@Id", new Song
            {
                Id = songId
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Song ""{songId}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li.First()
            };
        }

        /* ----------------[[ Vu ]]---------------- */
        [HttpPost("create")]
        public async Task<ActionResult<object>> create(JsonElement json)
        {
            try
            {
                Song song = json.GetObject<Song>(true);

                await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("Song", "Username", "Src", "Title", "Artist", "Genre", "Duration", "Thumbnail", "Cover", "Lyrics", "Description"), song);
                return new
                {
                    status = 0
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = -1,
                    message = ex.Message
                };
            }
        }

        //Quynh
        [HttpGet("get/user/{username}")]
        public async Task<ActionResult<object>> getCollection(string username)
        {
            var li = (await Program.Sql.QueryAsync<Song>("select * from Song s where s.Username = @Username and s.Status != 1 ", new Song
            {
                Username = username
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Song of ""{username}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li
            };
        }

        //Quynh
        [HttpDelete("{id}")]
        public async Task<ActionResult<object>> deleteCollection(int id)
        {
            try
            {
                await Program.Sql.ExecuteAsync("delete from Song where Song.Id = @Id ", new Song
                {
                    Id = id
                });

                return new
                {
                    Status = 0,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    Status = -1,
                    message = ex.Message
                };
            }
        }
        //Quynh
        [HttpPost("update/ispublic/{check:bool}/{songId:int}")]
        public async Task<ActionResult<object>> updatePubicCollection(bool check, int songId)
        {
            try
            {
                await Program.Sql.ExecuteAsync("update Song set IsPublic = @IsPublic where Id=@Id", new Song
                {
                    Id = songId,
                    IsPublic = check
                });

                return new
                {
                    Status = 0,
                };

            }
            catch (Exception ex)
            {
                return new
                {
                    Status = -1,
                    message = ex.Message
                };
            }
        }

        //Quynh
        [HttpPost("update")]
        public async Task<ActionResult<object>> updateSongCollection(JsonElement json)
        {
            try
            {
                Song song = json.GetObject<Song>(true);

                await Program.Sql.ExecuteAsync("update Song set Thumbnail=@Thumbnail, Cover=@Cover, Title=@Title, Artist=@Artist, Lyrics=@Lyrics, Description=@Description where Id=@Id", song);

                return new
                {
                    Status = 0,
                };

            }
            catch (Exception ex)
            {
                return new
                {
                    status = -1,
                    message = ex.Message
                };
            }

        }

        // Author: Nguyen Khanh Duy
        [HttpGet("get/top/{n:int}")]
        public async Task<ActionResult<object>> GetTop(int n)
        {
            var li = (await Program.Sql.QueryAsync<Song>("select top(@Top) * from Song where IsPublic = 'True' and Status != 1 order by Date desc", new { Top = n })).AsList();

            if (li.Count == 0) return new
            {
                Status = 0,
                Message = $@"Do not have any song",
            };

            return new
            {
                Status = 0,
                Data = li,
            };
        }

        // Author: Nguyen Khanh Duy
        [HttpGet("get/topgenre/{n:int}-{genre}")]
        public async Task<ActionResult<object>> GetTopGenre(int n, string genre)
        {
            var li = (await Program.Sql.QueryAsync<Song>("select top(@Top) * from Song where IsPublic = 'True' AND Genre = @Genre AND Status != 1 order by Date desc", new { Genre = genre, Top = n })).AsList();

            if (li.Count == 0) return new
            {
                Status = 0,
                Message = $@"Do not have any song",
            };
            return new
            {
                Status = 0,
                Data = li,
            };
        }
        // Author: Nguyen Khanh Duy
        // Get n genre and return list<string> genre
        [HttpGet("get/genre/{n:int}")]
        public async Task<ActionResult<object>> GetGenre(int n)
        {
            var li = (await Program.Sql.QueryAsync<string>("select top (@Top) s.Genre from (select distinct(Genre) from Song) s where s.Genre is not null", new { Top = n })).AsList();

            if (li.Count == 0) return new
            {
                Status = 0,
                Message = $@"Do not have any song",
            };
            return new
            {
                Status = 0,
                Data = li,
            };
        }

        //Search 
        //Author: Nguyen Khanh Duy
        [HttpGet("search/name/{word}")]
        public async Task<ActionResult<object>> GetTitle(string word)
        {
            var li = (await Program.Sql.QueryAsync<Song>("Select * from Song where IsPublic=1 AND Status != 1 and dbo.rmvAccent(Title) like concat(N'%',dbo.rmvAccent(@Word),'%')", new
            {
                Word = word
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Song with title: ""{word}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li,
            };
        }

        //Search 
        //Author: Nguyen Khanh Duy
        [HttpGet("search/lyric/{word}")]
        public async Task<ActionResult<object>> GetLyric(string word)
        {
            var li = (await Program.Sql.QueryAsync<Song>("Select * from Song where IsPublic = 'True' AND Status != 1 and dbo.rmvAccent(Lyrics) like concat(N'%',dbo.rmvAccent(@Word),'%')", new
            {
                Word = word
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Song with lyrics: ""{word}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li,
            };
        }

        //Search 
        //Author: Nguyen Khanh Duy
        [HttpGet("search/singer/{word}")]
        public async Task<ActionResult<object>> GetSinger(string word)
        {
            var li = (await Program.Sql.QueryAsync<Song>("Select * from Song where IsPublic = 'True' AND Status != 1 and dbo.rmvAccent(Artist) like concat(N'%',dbo.rmvAccent(@Word),'%')", new
            {
                Word = word
            })).AsList();

            if (li.Count == 0) return new
            {
                Status = -1,
                Message = $@"Song with lyrics: ""{word}"" not found"
            };

            return new
            {
                Status = 0,
                Data = li,
            };
        }


    }
}
