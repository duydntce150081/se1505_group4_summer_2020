﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MUZIC.Model;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace MUZIC.API.Controllers
{
    public class SettingController : ConBase
    {
        //Quynh
        [HttpPost("update/profile/{name}")]
        public async Task<ActionResult<object>> updateProfile(JsonElement json, string name)
        {
            try
            {
                Account account = json.GetObject<Account>(true);

                await Program.Sql.ExecuteAsync("update Account set  DisplayName=@DisplayName, Email=@Email where Username=@name", new
                {
                    Username = account.Username,
                    DisplayName = account.DisplayName,
                    Email = account.Email,
                    name = name
                });

                return new
                {
                    Status = 0,
                };

            }
            catch (Exception ex)
            {
                return new
                {
                    status = -1,
                    message = ex.Message
                };
            }

        }
        //Quynh
        [HttpPost("update/password/{username}/{oldpass}/{newpass}")]
        public async Task<ActionResult<object>> updatePassword(string username, string oldpass, string newpass)
        {
            try
            {
                var result = await Program.Sql.ExecuteAsync("update Account set Password=@NewPassword where Username=@Username and Password=@OldPassword", new
                {
                    Username = username,
                    NewPassword = UserController.getHash(newpass),
                    OldPassword = UserController.getHash(oldpass)
                });

                if (result == 0) throw new Exception("Incorect oldpassword!");

                return new
                {
                    Status = 0,
                };

            }
            catch (Exception ex)
            {
                return new
                {
                    status = -1,
                    message = ex.Message
                };
            }

        }
    }
}
