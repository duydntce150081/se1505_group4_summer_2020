document.addEventListener("DOMContentLoaded", () => {
    let queue = document.querySelector(".media-queue")
    let queueCloseBtn = document.querySelector(".queue-close-btn")
	let queueToggleBtn = document.querySelector(".media")
	let clear_btn = document.getElementById("clear_btn")
	let queue_list = document.querySelector(".list-song")
	
	let shuffle_btn = document.getElementById('mp3-shuffle')
	let prev_btn = document.getElementById('mp3-prev')
	let next_btn = document.getElementById('mp3-next')
	let play_btn = document.getElementById('mp3-play')
	let repeat_btn = document.getElementById('mp3-repeat')

	let progress_bar = document.getElementById('progress-song-input')
	let mp3_time_start = document.getElementById('mp3-time-start')
	let mp3_time_end = document.getElementById('mp3-time-end')

	let content_thumbnail = document.querySelector(".media-thumbnail");
	let content_title = document.querySelector(".media-content-title");
	let content_artist = document.querySelector(".media-content-author");

	queueCloseBtn.addEventListener("click", function (e) {
		queue.style.transform = "";
	})

	queueToggleBtn.addEventListener("click", function (e) {
		if (queue.style.transform === "") {
			queue.style.transform = "translateX(0)";
		} else queue.style.transform = "";
	})

	let mediaPlayer = document.querySelector(".media-player")
	let toggleMediaPlayerBtn = document.querySelector(".toggle-media-player")

	toggleMediaPlayerBtn.addEventListener("click", function (e) {
		mediaPlayer.classList.toggle("media-player-narrow");

		this.classList.toggle("fa-arrow-circle-right");
		this.classList.toggle("fa-arrow-circle-left");
	})

	function update_ui_buttons() {
		if (mp3.queue.length !== 0) {
			shuffle_btn.classList.remove('disable')
			play_btn.classList.remove('disable')
			repeat_btn.classList.remove('disable')
			progress_bar.disabled = null
		} else {
			shuffle_btn.classList.add('disable')
			play_btn.classList.add('disable')
			repeat_btn.classList.add('disable')
			progress_bar.disabled = true
		}

		if (mp3.queue.length > 1) {
			prev_btn.classList.remove('disable')
			next_btn.classList.remove('disable')
		} else {
			prev_btn.classList.add('disable')
			next_btn.classList.add('disable')
		}
	}

	//function shuffle(array) {
	//	for (let i = array.length - 1; i > 0; i--) {
	//		let j = Math.floor(Math.random() * (i + 1));
	//		[array[i], array[j]] = [array[j], array[i]];
	//	}
	//}

	function mapValue(v, x, y, X, Y) {
		return (v - x) / (y - x) * (Y - X) + X;
    }

	function get_time_format(m, s) {
		if (m < 10) m = "0" + m
		if (s < 10) s = "0" + s
		return m + ":" + s
	}

	function update_ui_progress() {
		let ratio = mp3.duration == 0 ? 0 : mp3.currentTime / mp3.duration * 100

		progress_bar.value = ratio
		progress_bar.style.background = `linear-gradient(to right, #f46530 0%, #f46530 ${ratio}%, #333 ${ratio}%, #333 100%)`

		let second = parseInt(mp3.currentTime) % 60
		let minute = parseInt(mp3.currentTime / 60)
		mp3_time_start.innerHTML = get_time_format(minute, second)
	}

	function update_ui_duration() {
		let second = parseInt(mp3.duration) % 60
		let minute = parseInt(mp3.duration / 60)
		mp3_time_end.innerHTML = get_time_format(minute, second)
	}

	function update_queue_active() {
		if (!mp3.queue.length) return;
		for (let i = 0; i < mp3.queue.length; ++i) {
			let item = queue_list.children[i];
			item.classList.remove("active");
		}
		queue_list.children[mp3.index].classList.add("active");
    }

	function update_ui_content() {
		update_queue_active();
		song = mp3.queue.length ? mp3.queue[mp3.index] : {
			thumbnail: "/img/upload-thumbnail.jpg",
			title: "(Empty queue)",
			artist: "Unknown"
        }
		content_thumbnail.src = song.thumbnail;
		content_title.innerHTML = song.title;
		content_artist.innerHTML = song.artist;
	}

	window.remove_queue = function (idx) {
		if (mp3.queue.length == 1) {
			clear_btn.click();
			return;
		}

		let needUpdate = false;
		if (mp3.index == idx) {
			mp3.pause();
			update_ui_pause();
			mp3.currentTime = mp3.duration = 0;
			update_ui_progress();
			needUpdate = true;
        }
		if (mp3.index > idx) --mp3.index;
		mp3.queue.splice(idx, 1);
		mp3.index = Math.min(mp3.queue.length - 1, mp3.index);
		load_queue();
		if (needUpdate && mp3.queue.length) mp3.dom.src = mp3.queue[mp3.index].src;
		update_ui_buttons();
		update_ui_content();
		localStorage["queue"] = JSON.stringify(mp3.queue);
		localStorage["index"] = mp3.index;
	}

	window.change_song = function (idx) {
		mp3.change(idx);
		update_ui_content();
		update_ui_play();
    }

	function get_queue_html(song, idx) {
		return `
		<div class="song-item">
            <img
				onclick="change_song(${idx})"
                src="${song.thumbnail}"
                alt=""
                class="song-img"
            />
            <div class="song-item-content">
                <h2 class="title">${song.title}</h2>
                <h2 class="author">${song.artist}</h2>
            </div>
            <div class="song-item-tool">
                <i class="fal fa-ellipsis-h"></i>
                <i class="fal fa-times-circle" onclick="remove_queue(${idx})"></i>
            </div>
        </div>`
	}

	function load_queue() {
		queue_list.innerHTML = "";
		let i = 0;
		for (let song of mp3.queue) {
			queue_list.innerHTML += get_queue_html(song, i++);
		}
	}

	let mp3 = {
		dom: document.getElementById('mp3-player'),

		load: function () {
			this.reset();

			if ("queue" in localStorage) {
				this.queue = JSON.parse(localStorage['queue']);
				this.index = parseInt(localStorage['index'])
				this.currentTime = parseFloat(localStorage['currentTime'] || 0)
				this.duration = parseFloat(localStorage['duration'] || 0)
				this.dom.src = this.queue[this.index].src
				this.dom.currentTime = this.currentTime
				load_queue();
				update_ui_progress();
				update_ui_duration();
			}
			if (this.queue.length > 0) update_ui_content();
			update_ui_buttons();
		},

		add: function (song) {
			queue_list.innerHTML += get_queue_html(song, mp3.queue.length);
			mp3.queue.push(song);
			if (mp3.queue.length == 1) {
				update_ui_content();
				mp3.dom.src = song.src;
				localStorage["index"] = this.index;
			}
			localStorage.setItem("queue", JSON.stringify(mp3.queue));
			update_ui_buttons();
        },

		reset: function () {
			this.pause();
			this.queue = [];
			this.index = 0;
			this.currentTime = 0;
			this.duration = 0;
			this.repeat_status = 0; // 0: none, 1: repeat playlist, 2: repeat song
			this.dom.src = "";
        },

		play: function () {
			this.dom.play()
			update_ui_content();
			audioCtx.resume();
			localStorage.setItem("index", this.index);
		},

		pause: function () {
			this.dom.pause()
		},

		next: function () {
			this.index = (this.index + 1) % this.queue.length
			this.dom.src = this.queue[this.index].src
			this.play()
		},

		prev: function () {
			this.index = (this.index - 1 + this.queue.length) % this.queue.length
			this.dom.src = this.queue[this.index].src
			this.play()
		},

		change: function (idx) {
			this.index = idx;
			this.dom.src = this.queue[this.index].src
			this.play();
        },

		queue_tmp: null,
		shuffle: function () {
			queue_tmp = queue
        }
	}

	mp3.dom.addEventListener("loadedmetadata", function () {
		mp3.duration = this.duration
		localStorage.setItem('duration', this.duration)
		update_ui_duration();
	})

	mp3.dom.addEventListener('timeupdate', (e) => {
		let currentTime = mp3.dom.currentTime
		mp3.currentTime = currentTime
		localStorage.setItem('currentTime', currentTime)
		update_ui_progress()
	});

	function update_ui_play() {
		play_btn.classList.add("fa-pause")
		play_btn.classList.remove("fa-play")
	}

	function update_ui_pause() {
		play_btn.classList.add("fa-play")
		play_btn.classList.remove("fa-pause")
	}

	mp3.dom.addEventListener("ended", function () {
		// 0: none, 1: repeat playlist, 2: repeat song
		if (mp3.repeat_status == 0 && mp3.index == mp3.queue.length - 1) {
			update_ui_pause();
			return;
		}
		if (mp3.repeat_status === 2) mp3.play();
		else mp3.next();
	})

	shuffle_btn.addEventListener("click", function () {
		if (this.classList.contains("disable")) return;
		mp3.shuffle()
		this.classList.toggle('active')
	})

	prev_btn.addEventListener("click", function () {
		if (this.classList.contains("disable")) return;
		mp3.pause();
		mp3.prev();
		update_ui_play();
	})

	next_btn.addEventListener("click", function () {
		if (this.classList.contains("disable")) return;
		mp3.pause();
		mp3.next();
		update_ui_play();
	})

	play_btn.addEventListener("click", function () {
		if (this.classList.contains("disable")) return;
		if (this.classList.contains('fa-play')) {
			if (mp3.repeat_status === 2 || !mp3.dom.ended) mp3.play();
			else mp3.next();
		}
		else mp3.pause();
		this.classList.toggle("fa-play");
		this.classList.toggle("fa-pause");
	})

	let repeat_btn_status = ['fa-repeat-alt', 'fa-repeat-alt', 'fa-repeat-1-alt']
	repeat_btn.addEventListener("click", function () {
		if (this.classList.contains("disable")) return;
		this.classList.remove(repeat_btn_status[mp3.repeat_status++])
		mp3.repeat_status %= 3
		if (mp3.repeat_status < 2) this.classList.toggle('active')
		this.classList.add(repeat_btn_status[mp3.repeat_status])
	})

	progress_bar.addEventListener('input', function (e) {
		mp3.dom.pause()
		mp3.dom.currentTime = this.value * mp3.duration / 100
	})

	progress_bar.addEventListener('change', function (e) {
		mp3.dom.pause()
		if (play_btn.classList.contains('fa-pause')) mp3.dom.play()
	})

	let mp3_volume = document.getElementById('player-control-volume')
	function update_volume(value) {
		value = parseFloat(value)
		mp3.dom.volume = value;
		mp3_volume.value = value;
		localStorage.setItem("volume", value);
		value *= 100;
		mp3_volume.style.background = `linear-gradient(to right, #f46530 0%, #f46530 ${value}%, #333 ${value}%, #333 100%)`
    }
	if (localStorage.getItem("volume") === null) localStorage.setItem("volume", 1);
	mp3_volume.addEventListener('input', e => update_volume(e.target.value));
	update_volume(localStorage.getItem("volume"));

	let mp3_mute = document.getElementById('mp3-mute');
	function update_mute() {
		if (localStorage.getItem("volume-mute") === null) {
			mp3_mute.classList.remove("fa-volume-mute");
			mp3_mute.classList.add("fa-volume-off");
		} else {
			mp3_mute.classList.remove("fa-volume-off");
			mp3_mute.classList.add("fa-volume-mute");
        }
		mp3.dom.muted = localStorage.getItem("volume-mute") !== null;
	}
	mp3_mute.addEventListener("click", () => {
		if (localStorage.getItem("volume-mute") === null) localStorage.setItem("volume-mute", "");
		else localStorage.removeItem("volume-mute");
		update_mute();
	});
	update_mute();

	clear_btn.addEventListener("click", () => {
		mp3.reset();
		queue_list.innerHTML = "";
		localStorage.removeItem("queue");
		localStorage.removeItem("index");
		localStorage.removeItem("currentTime");
		localStorage.removeItem("duration");
		update_ui_pause();
		update_ui_content();
		update_ui_progress();
		update_ui_duration();
		update_ui_buttons();
	})

	window.add_song = function(song) {
		const { id, thumbnail, title, artist, src } = song;
		song = { id, thumbnail, title, artist, src };
		mp3.add(song);
	}

	window.play_song = function(song) {
		clear_btn.click();
		add_song(song);
		play_btn.click();
	}

	window.add_playlist = function(list) {
		for (let song of list) add_song(song);
	}

	window.play_playlist = function (list) {
		if (list && list.length) {
			play_song(list[0]);
			for (let i = 1; i < list.length; ++i) add_song(list[i]);
        }
	}

	mp3.load();

	//------------- Moveable Visualizer -------------//
	let as_ctn = document.getElementById("audio-visual-container");
	if (localStorage["visualX"] !== undefined) {
		as_ctn.style.top = localStorage["visualY"];
		as_ctn.style.left = localStorage["visualX"];
	}

	let lastX, lastY;
	function elementDrag(e) {
		localStorage["visualY"] = as_ctn.style.top = (as_ctn.offsetTop - lastY + e.clientY) + "px";
		localStorage["visualX"] = as_ctn.style.left = (as_ctn.offsetLeft - lastX + e.clientX) + "px";
		lastX = e.clientX; lastY = e.clientY;
	}

	function closeDrag() {
		window.removeEventListener("mousemove", elementDrag);
		window.removeEventListener("mouseup", closeDrag);
    }

	as_ctn.addEventListener("mousedown", function (e) {
		lastX = e.clientX; lastY = e.clientY;
		window.addEventListener("mousemove", elementDrag);
		window.addEventListener("mouseup", closeDrag);
	});

	//------------- Audio Visualizer -------------//
	window.AudioContext = window.AudioContext || window.webkitAudioContext;
	const audioCtx = new AudioContext();
	const analyzer = audioCtx.createAnalyser();
	const source = audioCtx.createMediaElementSource(mp3.dom);
	source.connect(analyzer);
	analyzer.connect(audioCtx.destination);

	let canvas = document.getElementById("audio-visual-canvas");
	let pw = canvas.offsetWidth, ph = canvas.offsetHeight;
	canvas.style.width = canvas.style.height = "initial";
	canvas.width = 1000; canvas.height = 1000;
	canvas.style.left = `${pw/2 - canvas.width/2}px`;
	canvas.style.top = `${ph/2 - canvas.height/2}px`;
	let ctx = canvas.getContext("2d");

	let bufferLength, dataArray, colorVal = 0;
	let drawData = {
		circle_rect: {
			init: function () {
				analyzer.fftSize = 512;
				bufferLength = analyzer.frequencyBinCount;
				dataArray = new Uint8Array(bufferLength);
				this.deg = 0;
			},
			draw: function () {
				analyzer.getByteFrequencyData(dataArray);

				ctx.save();
				ctx.translate(canvas.width / 2, canvas.height / 2);
				this.deg += 0.025;
				if (this.deg > 360) this.deg -= 360;
				ctx.rotate(Math.PI + Math.PI / 180 * this.deg);

				let length = bufferLength/2, r = 100;
				for (let i = 0; i < length; ++i) {
					ctx.fillStyle = `hsl(${mapValue(dataArray[i], 0, 255, 180 + colorVal, 360 + colorVal)}, ${mapValue(dataArray[i], 0, 255, 30, 70)}%, 50%)`;
					let deg = 2 * Math.PI / bufferLength * i;
					// save before move around
					ctx.save();
					ctx.translate(r * Math.cos(deg), r * Math.sin(deg));
					// save before rotate a rectangle
					ctx.save();
					ctx.rotate(deg);
					ctx.fillRect(0, 0, 1.5, Math.max(1, dataArray[i] / 1.5));
					// restore before rotate
					ctx.restore();
					// restore before move
					ctx.restore();
				}

				for (let i = 0; i < length; ++i) {
					ctx.fillStyle = `hsl(${mapValue(dataArray[i], 0, 255, colorVal, 180 + colorVal)}, ${mapValue(dataArray[i], 0, 255, 30, 70)}%, 50%)`;
					let deg = 2 * Math.PI / bufferLength * (length + i);
					// save before move around
					ctx.save();
					ctx.translate(r * Math.cos(deg), r * Math.sin(deg));
					// save before rotate a rectangle
					ctx.save();
					ctx.rotate(deg);
					ctx.fillRect(0, 0, 1.5, Math.max(1, dataArray[i] / 1.5));
					// restore before rotate
					ctx.restore();
					// restore before move
					ctx.restore();
				}

				ctx.restore();
            }
		},
		waveform: {
			init: function () {
				analyzer.fftSize = 1024;
				bufferLength = analyzer.fftSize;
				dataArray = new Uint8Array(bufferLength);
			},
			draw: function () {
				analyzer.getByteTimeDomainData(dataArray);

				ctx.save();
				ctx.translate(canvas.width / 2, canvas.height / 2);
				ctx.strokeStyle = "white";

				let minR = 50, maxR = 150;
				ctx.beginPath();
				for (let i = 0; i < bufferLength; ++i) {
					let deg = 2 * Math.PI / bufferLength * i;
					let r = mapValue(dataArray[i], 0, 255, minR, maxR);
					let x = r * Math.cos(deg), y = r * Math.sin(deg);
					if (i == 0) ctx.moveTo(x, y);
					else ctx.lineTo(x, y);
				}
				ctx.closePath();
				ctx.stroke();

				ctx.restore();
			}
        }
	}

	let visualizerStyle = "circle_rect";
	window.change_visual_style = function(style) {
		visualizerStyle = style;
		drawData[style].init();
	}
	change_visual_style(visualizerStyle);

	let lastTick = Date.now()
	function updateDrawing() {
		let curTick = Date.now();
		if (curTick - lastTick > 166) {
			lastTick = curTick;
			colorVal = (colorVal + 1) % 3600;
		}
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		drawData[visualizerStyle].draw();
		requestAnimationFrame(updateDrawing);
	}
	updateDrawing();
});

