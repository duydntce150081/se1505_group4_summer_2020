﻿document.addEventListener("DOMContentLoaded", () => {
    const data = {
        datasets: [
            {
                label: 'Top1',
                borderColor: '#FF0404',
                fill: false,
                tension: 0.1,
                pointStyle: 'circle'
            },
            {
                label: 'Top2',
                borderColor: '#4C6EF5',
                fill: false,
                tension: 0.1,
                pointStyle: 'circle',
            },
            {
                label: 'Top3',
                borderColor: '#25A56A',
                fill: false,
                tension: 0.1,
                pointStyle: 'circle',
            },
            {
                label: 'Top4',
                borderColor: '#FFF506',
                fill: false,
                tension: 0.1,
                pointStyle: 'circle',
            },
            {
                label: 'Top5',
                borderColor: '#E62EB2',
                fill: false,
                tension: 0.1,
                pointStyle: 'circle',
            }
        ]
    };

    let config = {
        type: 'line',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
            },
            interaction: {
                intersect: false,
            },
            scales: {
                x: {
                    display: true,
                    title: {
                        display: true
                    },
                    grid: {
                        display: false
                    }
                },
                y: {
                    display: true,
                    grid: {
                        color: "#ffffff55",
                        borderDash: [2, 5]
                    },
                    ticks: {
                        display: false
                    },
                    suggestedMin: 0,
                    suggestedMax: 1
                }
            }
        },
    };

    let suggestedMax = 1

    function addData(chart, idx, data) {
        chart.data.labels.push((idx * 2 + 2).toString().padStart(2, 0));
        for (let i = 0; i < data.length; ++i) {
            chart.data.datasets[i].data.push(data[i][idx]);
            suggestedMax = Math.max(suggestedMax, data[i][idx]);
        }
        chart.config.options.scales.y.suggestedMax = suggestedMax;
        chart.update();
    }

    let myChart = null;
    window.show_chart = (data, dataLabel) => {
        if (myChart !== null) myChart.destroy();
        myChart = new Chart(
            document.getElementById('myChart'),
            config
        );

        myChart.data.labels = []
        for (let i = 0; i < data.length; ++i) {
            myChart.data.datasets[i].data = [];
            myChart.data.datasets[i].label = dataLabel[i];
        }

        for (let i = 0; i < 12; ++i)
            setTimeout(() => {
                addData(myChart, i, data)
            }, 20 * i)

        myChart.options.plugins.title.align = 'start';
        myChart.update();
    }
});