using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MUZIC.App.Service;
using MUZIC.Model;

namespace MUZIC.App.Pages.Feedback
{
    public class feedbackModel : PageModel
    {
        public Account? account = new();
        public async Task<IActionResult> OnGet()
        {
            account = await State.GetAccount(HttpContext)!;
            //if (_account is not null) return Redirect("/profile");
            return Page();
        }

        public async Task<IActionResult> OnPost(string title, string message)
        {
            account = await State.GetAccount(HttpContext)!;
            TempData["message"] = true;
            await FeedbackService.Send(account!.Username, title, message);
            return Page();
        }   
    }
}
