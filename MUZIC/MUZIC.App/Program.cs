using Imagekit;
using MUZIC.App.Service;
using RestSharp;

namespace MUZIC.App;

class Program
{
    public static RestClient Client { get; set; } = null!;

    public static IConfiguration Config { get; private set; } = null!;

    public static ServerImagekit Imagekit
    {
        get => new(Program.Config["Imagekit:PublicKey"], Program.Config["Imagekit:PrivateKey"], Program.Config["Imagekit:Url"]);
    }

    public static string RootPath { get; set; } = null!;

    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddRazorPages();
        builder.Services.AddServerSideBlazor();

        builder.Services.AddHttpContextAccessor();
        builder.Services.AddScoped<State>();
        builder.Services.AddScoped<Mp3>();

        var app = builder.Build();

        Config = app.Configuration;
        Program.Client = new RestClient(Config["APIServer"]);
        RootPath = app.Environment.ContentRootPath;
        Console.WriteLine(RootPath);

        // Configure the HTTP request pipeline.
        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Error");
        }

        app.UseStaticFiles();
        app.UseRouting();
        app.MapBlazorHub();
        app.UseMiddleware<SimpleMiddleware>();
        app.MapFallbackToPage("/_Host");

        app.Run();
    }
}
