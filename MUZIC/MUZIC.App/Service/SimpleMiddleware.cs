﻿namespace MUZIC.App.Service
{
    public class SimpleMiddleware
    {
        private readonly RequestDelegate _next;

        public SimpleMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            string? path = context.GetEndpoint()?.ToString();
            if (path is not null && (path == "/_Host" || path == "/_blazor"))
            {
                var acc = await State.GetAccount(context);
                if (acc is not null) context.Items["_acc"] = acc;
            }
            await _next(context);
        }
    }
}
