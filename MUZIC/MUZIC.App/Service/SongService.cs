﻿using MUZIC.Model;
using RestSharp;
using System.Text.Json;
using MUZIC.App.Utility.Extension;

namespace MUZIC.App.Service
{
    public class SongService
    {
        public static async Task<Song?> GetSong(int songId)
        {
            var request = new RestRequest($"/song/get/{songId}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<Song>();
                return r;
            }
            return null;
        }

        public static async Task Create(Song song)
        {
            var request = new RestRequest($"/song/create", Method.Post);
            request.AddJsonBody(song);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }

        //Quynh
        public static async Task<List<Song>?> GetCollection(string username)
        {
            var request = new RestRequest($"/song/get/user/{username}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Song>>();
                return r;
            }
            return null;
        }
        //Quynh
        public static async Task DeleteCollection(int id)
        {
            var request = new RestRequest($"/song/{id}", Method.Delete);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }
        //Quynh
        public static async Task UpdatePubicCollection(bool check, int songId)
        {
            var request = new RestRequest($"/song/update/ispublic/{check}/{songId}", Method.Post);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }
        //Quynh
        public static async Task UpdateSongCollection(Song song)
        {
            var request = new RestRequest($"/song/update", Method.Post);
            request.AddJsonBody(song);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }

        // Author: Nguyen Khanh Duy
        public static async Task<List<Song>?> GetTop(int n)
        {
            var request = new RestRequest($"/song/get/top/{n}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Song>?>();
                return r;
            }
            return null;
        }
        // Author: Nguyen Khanh Duy

        public static async Task<List<Song>?> GetTopGenre(int n, string genre)
        {
            var request = new RestRequest($"/song/get/topgenre/{n}-{genre}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Song>?>();
                return r;
            }
            return null;
        }
        // Author: Nguyen Khanh Duy

        public static async Task<List<string>?> GetGenre(int n)
        {
            var request = new RestRequest($"/song/get/genre/{n}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<string>?>();
                return r;
            }
            return null;
        }

        //Search
        // Author: Nguyen Khanh Duy
        public static async Task<List<Song>?> GetTitle(string? word)
        {
            var request = new RestRequest($"/song/search/name/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Song>?>();
                return r;
            }
            return null;
        }

        //Search
        // Author: Nguyen Khanh Duy
        public static async Task<List<Song>?> GetLyric(string? word)
        {
            var request = new RestRequest($"/song/search/lyric/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Song>?>();
                return r;
            }
            return null;
        }

        //Search
        // Author: Nguyen Khanh Duy
        public static async Task<List<Song>?> GetSinger(string? word)
        {
            var request = new RestRequest($"/song/search/singer/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Song>?>();
                return r;
            }
            return null;
        }
    }
}
