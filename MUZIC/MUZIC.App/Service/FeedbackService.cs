﻿using MUZIC.Model;
using RestSharp;

namespace MUZIC.App.Service
{
    public class FeedbackService
    {
        public static async Task Send(string username, string title, string detail)
        {
            var request = new RestRequest("feedback/send", Method.Post);
            request.AddJsonBody(new { username, title, detail });
            var res = await Program.Client.ExecuteAsync(request);
            if (!res.IsSuccessful) Console.WriteLine("Feedback.Send: failed!");
        }
    }
}
