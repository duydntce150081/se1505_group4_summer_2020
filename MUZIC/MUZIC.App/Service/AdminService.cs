﻿using MUZIC.Model;
using RestSharp;
using System.Text.Json;
using MUZIC.App.Utility.Extension;

namespace MUZIC.App.Service
{
    public class AdminService
    {
        //Duydn
        public static async Task<int> GetListAccountCnt(string word)
        {
            var request = new RestRequest($"admin/get/users_cnt/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            return json.GetProperty("data").GetInt32();
        }

        public static async Task<List<Account>> GetListAccount(int page_id, int item_per_page, string word)
        {
            var request = new RestRequest($"admin/get/users/{page_id}/{item_per_page}/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on get all song");
                return null!;
            }
            return json.GetProperty("data").GetObject<List<Account>>();
        }

        public static async Task<int> GetListSongCnt(string word)
        {
            var request = new RestRequest($"admin/get/songs_cnt/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            return json.GetProperty("data").GetInt32();
        }

        public static async Task<List<Song>> GetListSong(int page_id, int item_per_page, string word)
        {
            var request = new RestRequest($"admin/get/songs/{page_id}/{item_per_page}/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on get all song");
                return null!;
            }
            return json.GetProperty("data").GetObject<List<Song>>();
        }

        public static async Task<int> GetListFeedbackCnt(string word)
        {
            var request = new RestRequest($"admin/get/feedbacks_cnt/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            return json.GetProperty("data").GetInt32();
        }

        public static async Task<List<Feedback>> GetListFeedback(int page_id, int item_per_page, string word)
        {
            var request = new RestRequest($"admin/get/feedbacks/{page_id}/{item_per_page}/{word}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on get all song");
                return null!;
            }
            return json.GetProperty("data").GetObject<List<Feedback>>();
        }

        public static async Task UpdateFeedbackPriority(int id, int priority) 
        {
            var request = new RestRequest($"admin/update/feedbacks", Method.Post);
            request.AddObject(new { id, priority });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on UpdateFeedbackPriority");
            }
        }

        public static async Task UpdateAccountStatus(string Username, int Status)
        {
            var request = new RestRequest($"admin/update/accounts", Method.Post);
            request.AddObject(new { Username, Status });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on UpdateAccountStatus");
            }
        }

        public static async Task UpdateSongStatus(int Id, int Status)
        {
            var request = new RestRequest($"admin/update/songs", Method.Post);
            request.AddObject(new { Id, Status });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on UpdateAccountStatus");
            }
        }


        public static async Task<DashboardMetric> GetDashboardMetric()
        {
            var request = new RestRequest($"admin/get/dashboardmetric", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on get dashboardmetric");
                return null!;
            }
            return json.GetProperty("data").GetObject<DashboardMetric>();
        }

        public static async Task<List<string>> GetDashboardFeedback()
        {
            var request = new RestRequest($"admin/get/dashboardfeedbacks", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on get all feedback");
                return null!;
            }
            return json.GetProperty("data").GetObject<List<string>>();
        }


        public static async Task InsertWebtracking(string? Username, string ContextURL)
        {
            var request = new RestRequest($"admin/insert/webtracking", Method.Post);
            request.AddObject(new { Username, ContextURL });
            var res = await Program.Client.ExecuteAsync(request);
            if (!res.IsSuccessful)
            {
                Console.WriteLine($"admin error on InsertWebtracking. StatusCode: {res.StatusCode}");
            }
        }
        public static async Task<List<int>> GetDashboardTrafic(string date)
        {
            var request = new RestRequest($"admin/get/dashboardtrafic/{date}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
            {
                Console.WriteLine("admin error on get all feedback");
                return null!;
            }
            return json.GetProperty("data").GetObject<List<int>>();
        }

        public static async Task<(int cnt_song, string last_date)> GetUseruploadinfo(string Username)
        {
            var request = new RestRequest($"admin/get/useruploadinfo/{Username}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            return (json.GetProperty("data").GetProperty("cnt_song").GetInt32(),
                json.GetProperty("data").GetProperty("last_date").GetString()!);
        }
    }
}
