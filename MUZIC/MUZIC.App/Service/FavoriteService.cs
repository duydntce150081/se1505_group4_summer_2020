﻿using MUZIC.Model;
using RestSharp;
using System.Text.Json;
using MUZIC.App.Utility.Extension;

namespace MUZIC.App.Service
{
    public class FavoriteService
    {
        //Quynh
        public static async Task<List<Song>> GetFavoriteSong(string name)
        {
            var request = new RestRequest($"favorite/get/list/song/{name}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Song>>();
                return r;
            }
            return new List<Song>();
        }

        //Quynh
        public static async Task<List<Playlist>> GetFavoritePlaylist(string name)
        {
            var request = new RestRequest($"favorite/get/list/playlist/{name}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Playlist>>();
                return r;
            }
            return new List<Playlist>();
        }

        //Quynh
        public static string PlaylistDuration(List<Song> list)
        {
            int secs = 0;
            string answer;

            for (int i = 0; i < list.Count; i++)
            {
                secs += list[i].Duration;
            }

            answer = SongDuration(secs);
            return answer;
        }
        //Quynh
        public static string SongDuration(int secs)
        {
            string answer;
            TimeSpan t = TimeSpan.FromSeconds(secs);

            if (t.Hours != 0)
            {
                answer = string.Format("{0:D2}:{1:D2}:{2:D2}",
                           t.Hours,
                           t.Minutes,
                           t.Seconds);
            }
            else
            {
                answer = string.Format("{0:D2}:{1:D2}",
                           t.Minutes,
                           t.Seconds);
            }
            return answer;
        }

        // Canh
        public static async Task<bool> IsSongFavorited(string username, int id)
        {
            var request = new RestRequest($"/favorite/check/song/{username}/{id}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<bool>();
                return r;
            }
            return false;
        }

        // Canh
        public static async Task<bool> enableSong(string username, int songId)
        {
            var request = new RestRequest($"/favorite/enable/song", Method.Post);
            request.AddObject(new { username, songId });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                return true;
            }
            return false;
        }

        // Canh
        public static async Task<bool> disableSong(string username, int songId)
        {
            var request = new RestRequest($"/favorite/disable/song", Method.Post);
            request.AddObject(new { username, songId });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
        
            if (json.GetProperty("status").GetInt16() == 0)
            {
                return false;
            }
            return true;
        }

        // Canh
        public static async Task<bool> enablePlaylist(string username, int playlistId)
        {
            var request = new RestRequest($"/favorite/enable/playlist", Method.Post);
            request.AddObject(new { username, playlistId });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                return true;
            }
            return false;
        }

        // Canh
        public static async Task<bool> disablePlaylist(string username, int playlistId)
        {
            var request = new RestRequest($"/favorite/disable/playlist", Method.Post);
            request.AddObject(new { username, playlistId });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                return false;
            }
            return true;
        }

        // Author: Nguyen Khanh Duy
        public static async Task<bool> IsPlaylistFavorite(string username, int id)
        {
            var request = new RestRequest($"/favorite/check/playlist/{username}/{id}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;

            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<bool>();
                return r;
            }
            return false;
        }
    }
}
