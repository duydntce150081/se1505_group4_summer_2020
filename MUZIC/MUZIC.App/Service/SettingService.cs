﻿using MUZIC.Model;
using RestSharp;
using System.Text.Json;
using MUZIC.App.Utility.Extension;

namespace MUZIC.App.Service
{
    public class SettingService
    {
        //Quynh
        public static async Task UpdateProfile(Account account, string name)
        {
            var request = new RestRequest($"/setting/update/profile/{name}", Method.Post);
            request.AddJsonBody(account);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }

        //Quynh
        public static async Task<string?> UpdatePassword(string username, string oldpass, string newpass)
        {
            var request = new RestRequest($"/setting/update/password/{username}/{oldpass}/{newpass}", Method.Post);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                return json.GetProperty("message").GetString();
            return null;
        }

        public static async Task UpdateAvatar(string username, string img)
        {
            var request = new RestRequest($"/user/avatar", Method.Post);
            request.AddJsonBody(new { username = username, img = img });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }
    }
}
