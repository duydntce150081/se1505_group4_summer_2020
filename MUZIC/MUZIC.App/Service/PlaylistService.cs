﻿using MUZIC.Model;
using RestSharp;
using System.Text.Json;
using MUZIC.App.Utility.Extension;

namespace MUZIC.App.Service
{
    public class PlaylistService
    {
        public static async Task<Playlist?> GetPlaylist(int playlistId)
        {
            var request = new RestRequest($"/playlist/get/info/{playlistId}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<Playlist>();
                return r;
            }
            return null;
        }

        // Author: Nguyen Khanh Duy
        public static async Task<List<Playlist>?> GetTop(int n)
        {
            var request = new RestRequest($"/playlist/get/top/{n}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Playlist>?>();
                return r;
            }
            return null;
        }

        //Search
        // Author: Nguyen Khanh Duy
        public static async Task<List<Playlist>?> GetTitle(string? title)
        {
            var request = new RestRequest($"/playlist/search/{title}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            //Console.WriteLine(json);
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Playlist>?>();
                return r;
            }
            return null;
        }


        //Quynh
        public static async Task<List<Playlist>?> GetCollection(string username)
        {
            var request = new RestRequest($"/playlist/get/user/{username}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Playlist>>();
                return r;
            }
            return null;
        }

        //Quynh
        public static async Task DeleteCollection(int id)
        {
            var request = new RestRequest($"/playlist/{id}", Method.Delete);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }
        //Quynh
        public static async Task DeletePlaylistSong(int PlaylistId, int SongId)
        {
            var request = new RestRequest($"/playlist/delete/song/{PlaylistId}/{SongId}", Method.Delete);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }
        //Quynh
        public static async Task UpdatePubicCollection(bool check, int playlistId)
        {
            var request = new RestRequest($"/playlist/update/ispublic/{check}/{playlistId}", Method.Post);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }

        //Quynh
        public static async Task UpdatePlaylistCollection(Playlist playlist, List<SongChoosen> list)
        {
            var request = new RestRequest($"/playlist/update", Method.Post);
            request.AddJsonBody(new { playlist = playlist, list = list });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }

        //Quynh
        public static async Task InsertPlaylist(Playlist playlist, List<SongChoosen> list, String username)
        {
            var request = new RestRequest($"/playlist/insert", Method.Post);
            request.AddJsonBody(new { playlist = playlist, list = list, username = username });
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }


        //Quynh
        public static async Task<List<SongChoosen>> GetListChoosen(string username, int Id, int type)
        {
            var request = new RestRequest($"/playlist/get/listchoosen/{username}/{Id}/{type}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            return json.GetProperty("data").GetObject<List<SongChoosen>>();
        }

        // Canh
        public static async Task<List<Song>> GetSongs(int? id)
        {
            var request = new RestRequest($"/playlist/get/songs/{id}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<List<Song>>();
                return r;
            }
            return new List<Song>();
        }

        public static async Task<int> GetDuration(int id)
        {
            var request = new RestRequest($"playlist/get/duration/{id}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            return json.GetProperty("data").GetInt32();
        }

        //Khanh Duy
        public static async Task InsertSong(Playlist playlist, Song song)
        {
            var request = new RestRequest($"/playlist/insert/song", Method.Post);
            request.AddJsonBody(new { playlist = playlist, song = song});
            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() != 0)
                Console.WriteLine(json.GetProperty("message").GetString());
        }
    }
}
