﻿using RestSharp;

namespace MUZIC.App
{
    public class TestHelper
    {
        private static bool first = false;
        public static void Enable()
        {
            if (first) return;
            first = true;
            Console.WriteLine("TestHelper.Enable()");

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory());
            dir = dir.Parent!.Parent!.Parent!.Parent!;

            Program.RootPath = $@"{dir.FullName}\MUZIC.App\";
            Program.Client = new RestClient(config["APIServer"]);
        }
    }
}
